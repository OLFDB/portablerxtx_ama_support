/*-------------------------------------------------------------------------
    Portable Library Loader License v 1.1 - LGPL v 3.0
    Portable Library Loader is a library that helps embedding naive code 
    in java JAR files.
    Copyright 2012 by Stefano Speretta
 
    A copy of the LGPL v 3.0 may be found at
    http://www.gnu.org/licenses/lgpl.txt on April 9th 2012.
 
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
--------------------------------------------------------------------------*/
package gnu.io;

/**
* @author Stefano Speretta
* @version 1.1
*/

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

public class PortableLibraryLoader
{
    private static final String EXTENSION_UNIX = ".so";
    private static final String EXTENSION_WIN = ".dll";
    private static final String EXTENSION_MAC = ".jnilib";
    private static final String WINDOWS = "win";
    private static final String LINUX = "linux";
    private static final String MAC = "mac";
    private static final String SOLARIS = "solaris";
    // list of libraries 
    private static HashMap<String, String> libraryList = new HashMap<String, String>();

    /**
     * Loads a code file with the specified filename from from a classpath
     * as a dynamic library. The filename must contain only the library name without 
     * extension or system specific suffixes (ex. libXXX under Unix systems).
     *
     * As an example, considering library Foo under classpth org.Foo.Native
     * 
     * The library can be loaded by calling 
     * 
     *	    PortableLibraryLoader.loadLibrary("/org/Foo/Native", "Foo");
     * 
     * Files should be stored in the JAR file using the following 
     * directory structure:
     * 
     *	<pre>org.Foo.Native
     *        +-- win
     *        |   +-- x86
     *        |   |   +-- Foo.dll
     *        |   +-- amd64
     *        |       +-- Foo.dll
     *        +-- linux
     *             +-- i386
     *             |   +-- libFoo.so
     *             +-- amd64
     *                 +-- libFoo.so
     * </pre>
     * 
     * @param      classpath the path to the library inside the JAR file.
     * @param      libname the file to load.
     * @param      reuse re-uses a previously unpacked library, if the name matches.
     * 
     * @exception  SecurityException  if a security manager exists and its
     *             <code>checkLink</code> method doesn't allow
     *             loading of the specified dynamic library
     * @exception  UnsatisfiedLinkError  if the file does not exist.
     * @exception  NullPointerException if <code>filename</code> is
     *             <code>null</code>
     * @see        java.lang.Runtime#load(java.lang.String)
     * @see        java.lang.SecurityManager#checkLink(java.lang.String)
     */
    public static void loadLibrary(String classpath, String libname, boolean reuse)
    {
	System.load(PortableLibraryLoader.getLibrary(classpath, libname, reuse));
    }
    
    /**
     * Loads a code file with the specified filename from from a classpath
     * as a dynamic library. The filename must contain only the library name without 
     * extension or system specific suffixes (ex. libXXX under Unix systems).
     * If a library is loaded multiple times, only one library is loaded and the 
     * same file is reused.
     * 
     * As an example, considering library Foo under classpth org.Foo.Native
     * 
     * The library can be loaded by calling 
     * 
     *	    PortableLibraryLoader.loadLibrary("/org/Foo/Native", "Foo");
     * 
     * Files should be stored in the JAR file using the following 
     * directory structure:
     * 
     *	<pre>org.Foo.Native
     *        +-- win
     *        |   +-- x86
     *        |   |   +-- Foo.dll
     *        |   +-- amd64
     *        |       +-- Foo.dll
     *        +-- linux
     *             +-- i386
     *             |   +-- libFoo.so
     *             +-- amd64
     *                 +-- libFoo.so
     * </pre>
     * 
     * @param      classpath the path to the library inside the JAR file.
     * @param      libname the file to load.
     * 
     * @exception  SecurityException  if a security manager exists and its
     *             <code>checkLink</code> method doesn't allow
     *             loading of the specified dynamic library
     * @exception  UnsatisfiedLinkError  if the file does not exist.
     * @exception  NullPointerException if <code>filename</code> is
     *             <code>null</code>
     * @see        java.lang.Runtime#load(java.lang.String)
     * @see        java.lang.SecurityManager#checkLink(java.lang.String)
     */
    public static void loadLibrary(String classpath, String libname)
    {
	System.load(PortableLibraryLoader.getLibrary(classpath, libname, true));
    }
    
    private static String getLibrary(String classpath, String libname, boolean reuse)
    {
	String os = getOS();
	String arch = getArchitecture();
	String path = classpath + "/" + os + "/" + arch;

	String extension = getExtension(os);

	String lib = getLibName(libname, os);

	if (reuse)
	{
	    String found = libraryList.get(lib);
	    if (found != null)
	    {
		return found;
	    }
	}
	
	String tmpFile = createTempFile(path, lib, extension, os, arch);
	if (reuse)
	{
	    libraryList.put (lib, tmpFile);
	}
	
	return tmpFile;
    }

    private static String getOS()
    {
	String JavaOS = System.getProperty("os.name").toLowerCase();

	String os = "";

	if (JavaOS.indexOf(WINDOWS) >= 0)
	{
	    // Windows
	    os = WINDOWS;
	}
	else if (JavaOS.indexOf("nix") >= 0 || JavaOS.indexOf("nux") >= 0)
	{
	    // Linux or LINUX
	    os = LINUX;
	}
	else if (JavaOS.indexOf(MAC) >= 0)
	{
	    // MAC
	    os = MAC;
	}
	else if ((JavaOS.indexOf("sunos") >= 0) || (JavaOS.indexOf(SOLARIS) >= 0))
	{
	    os = SOLARIS;
	}

	return os;
    }

    private static String getArchitecture()
    {
	return System.getProperty("os.arch").toLowerCase();
    }

    private static String getExtension(String os)
    {
	if (os.equals(WINDOWS))
	{
	    return EXTENSION_WIN;
	}
	else if (os.equals(MAC))
	{
	    return EXTENSION_MAC;
	}
	else
	{
	    return EXTENSION_UNIX;
	}
    }

    private static String getLibName(String libname, String os)
    {
	if (os.equals(LINUX) || os.equals(MAC) || os.equals(SOLARIS))
	{
	    return "lib" + libname;
	}
	else
	{
	    return libname;
	}
    }

    private static void deleteTempFiles(final String name, final String extension)
    {
	try
	{
	    File dir = new File(System.getProperty("java.io.tmpdir"));

	    File[] files = dir.listFiles(new FilenameFilter()
	    {
		@Override
		public boolean accept(File dir, String filename)
		{
		    return filename.matches(name + "[0-9]+" + extension);
		}
	    });

	    for (int i = 0; i < files.length; i++)
	    {
		files[i].delete();
	    }
	} catch (SecurityException ex)
	{
	    // file cannot be deleted, do not throw exception
	}

    }

    /**
     * Puts library to temp dir and loads to memory
     */
    private static String createTempFile(String path, String name, String extension,
					 String os, String arch)
    {
	try
	{
		
		System.out.println("Creating temp file: " + path + name + extension + os + arch);
	    // delete old temp files
	    deleteTempFiles(name, extension);

	    // read stream from Jar file
	    InputStream in = PortableLibraryLoader.class.getResourceAsStream(path + "/" + name + extension);

	    // create temporary library
	    File temp = File.createTempFile(name, extension);

	    // Delete temp file when program exits.
	    //temp.deleteOnExit();

	    FileOutputStream fos = new FileOutputStream(temp);
	    byte[] buffer = new byte[1024];
	    int bytesRead;

	    while ((bytesRead = in.read(buffer)) != -1)
	    {
		fos.write(buffer, 0, bytesRead);
	    }
	    fos.flush();
	    in.close();
	    fos.close();

	    return temp.getAbsoluteFile().toString();
	} catch (FileNotFoundException e)
	{
	    UnsatisfiedLinkError newex = new UnsatisfiedLinkError("Error creating temporary file");
	    newex.setStackTrace(e.getStackTrace());
	    throw newex;
	} catch (IOException e)
	{
	    UnsatisfiedLinkError newex = new UnsatisfiedLinkError("Error creating temporary file");
	    newex.setStackTrace(e.getStackTrace());
	    throw newex;
	} catch (NullPointerException e)
	{
	    UnsatisfiedLinkError newex = new UnsatisfiedLinkError("Unsupported OS / architecture (" + os + " / " + arch + ")");
	    newex.setStackTrace(e.getStackTrace());
	    throw newex;
	}
    }
}
